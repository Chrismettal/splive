# Splive <!-- omit in toc -->

[![PyPI - Version](https://img.shields.io/pypi/v/splive?style=flat-square)](https://pypi.org/project/splive/)
[![PyPI - License](https://img.shields.io/pypi/l/splive?style=flat-square)](https://pypi.org/project/splive/)
[![PyPI - Python Version](https://img.shields.io/pypi/pyversions/splive?style=flat-square)](https://pypi.org/project/splive/)

This is a work in progress.

Currently this script will read in multiple CSV files in alphabetical order and line by line write them into one spliced file, ommitting the header for all files but the first one. Useful for splicing logfiles from several days into one file.

Will get significant updates for interactive splicing etc.

If you like my work please consider supporting my caffeine addiction!

<a href='https://ko-fi.com/U7U6G0X3' target='_blank'><img height='36' style='border:0px;height:36px;' src='https://az743702.vo.msecnd.net/cdn/kofi4.png?v=0' border='0' alt='Buy Me a Coffee at ko-fi.com' /></a>
